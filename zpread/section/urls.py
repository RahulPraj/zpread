from django.urls import path
from .import views
from .views import delete_view


urlpatterns = [


    path('',views.home,name='section-home'),
    path('subject/manager/<int:pk>/', views.subject_manager, name='subject-manager'),
    path('course/update/<int:pk>/', views.course_update, name='course-update'),
    path('<id>/delete', delete_view,name='delete'), 
    path('course/add/', views.course_add, name='course-add'),
]


