from django.shortcuts import render

# Create your views here.

def home(request):
    context = {
        'courses' : Course.objects.all()
    }
    return render(request,'section/home.html',context)


from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import ListView,CreateView,DeleteView
from .models import Course,Subject,Chapter
from .forms import CourseAddForm, SubjectAddForm, ChapterAddForm
# Create your views here.

#def home1(request):
    #return render(request,'course/home1.html)


from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def course_add(request):
	if request.method == 'POST':
		form = CourseAddForm(request.POST)
		if form.is_valid():
			course = form.save(commit=False)
			# lesson.editable = True
			course.save()
			return redirect('section-home')
	else:
		form = CourseAddForm()
	context = {
	'form' : form,
	'route' : 1,
	}
	return render(request, 'section/course_add.html', context)
@login_required
@staff_member_required
def course_update(request, pk):
	# if not ( request.user.user_type == 1 or request.user.user_type == 2 ):
	# 	return redirect('staff-home')
	course = Course.objects.get(id=pk)
	if request.method == 'POST':
		form = CourseAddForm(request.POST, instance=course)
		if form.is_valid():
			form.save()
			return redirect('course-home')
	else:
		form = CourseAddForm(instance=course)
	context = {
	'form' : form,
	'route' : 2,
	}
	return render(request, 'course/course_add.html', context)
@login_required
@staff_member_required
def subject_manager(request, pk):
	course = Course.objects.get(id=pk)
	subjects = Subject.objects.filter(course=course)
	context = {
	'course' : course,
	'subjects' : subjects,
	}
	return render(request, 'course/subject.html', context)
@login_required
@staff_member_required
def subject_add(request, pk):
	course = Course.objects.get(id=pk)
	if request.method == 'POST':
		form = SubjectAddForm(request.POST, request.FILES)
		if form.is_valid():
			subject = form.save(commit=False)
			subject.course = course
			subject.save()
			#update_course_child_count(course)
			return redirect('subject-manager', course.id)
	else:
		form = SubjectAddForm()
	context = {
	'course' : course,
	'form' : form,
	'route' : 1,
	}
	return render(request, 'course/subject_add.html', context)
@login_required
@staff_member_required
def subject_update(request, pk):
	subject = Subject.objects.get(id=pk)
	if request.method == 'POST':
		form = SubjectAddForm(request.POST, request.FILES, instance=subject)
		if form.is_valid():
			form.save()
			return redirect('subject-manager', subject.course.id)
	else:
		form = SubjectAddForm(instance=subject)
	context = {
	'subject' : subject,
	'form' : form,
	'route' : 2,
	}
	return render(request, 'course/subject_add.html', context)
@login_required
@staff_member_required
def chapter_manager(request, pk):
	subject = Subject.objects.get(id=pk)
	chapters = Chapter.objects.filter(subject=subject)
	context = {
	'subject' : subject,
	'chapters' : chapters,
	}
	return render(request, 'course/chapter_manager.html', context)


from django.shortcuts import (get_object_or_404, 
							render, 
							HttpResponseRedirect) 

from .models import Course


# delete view for details 
def delete_view(request, id): 
	# dictionary for initial data with 
	# field names as keys 
	context ={} 

	# fetch the object related to passed id 
	obj = get_object_or_404(Course, id = id) 


	if request.method =="POST": 
		# delete object 
		obj.delete() 
		# after deleting redirect to 
		# home page 
		return HttpResponseRedirect("/") 

	return render(request, "section/delete_view.html", context) 
