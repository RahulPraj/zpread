from django import forms
from .models import Course, Subject, Chapter




class CourseAddForm(forms.ModelForm):
	class Meta:
		model = Course
		fields = '__all__'
		

class SubjectAddForm(forms.ModelForm):
	class Meta:
		model = Subject
		fields = '__all__'

class ChapterAddForm(forms.ModelForm):
	class Meta:
		model = Chapter
		fields = '__all__'
		