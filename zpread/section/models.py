from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone




class Course(models.Model):
	title = models.CharField(max_length=500)
    
    
	

class Subject(models.Model):
	course = models.ForeignKey(Course, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)


class Chapter(models.Model):
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
    
    



